package de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.boundary;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class KafkaTestHelper {
    private static final Duration TIMEOUT = Duration.ofSeconds(40);
    private final KafkaProducer<String, String> kafkaProducer;

    public KafkaTestHelper() {
        kafkaProducer = KafkaFactory.createProducer();
    }

    public void close() {
        kafkaProducer.close();
    }

    public void publish(final String topic, final String payload, final Map<String, String> header) {

        final List<Header> messageHeader = new ArrayList<>();
        for (final Map.Entry<String, String> entry : header.entrySet()) {
            messageHeader.add(new RecordHeader(entry.getKey(), entry.getValue().getBytes(StandardCharsets.UTF_8)));
        }

        var send = kafkaProducer.send(new ProducerRecord<>(topic, null, (String) null, payload, messageHeader));
        try {
            send.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
