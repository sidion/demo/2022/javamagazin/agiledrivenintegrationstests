package de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.boundary;

import au.com.dius.pact.consumer.MessagePactBuilder;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.consumer.junit5.ProviderType;
import au.com.dius.pact.core.model.PactSpecVersion;
import au.com.dius.pact.core.model.annotations.Pact;
import au.com.dius.pact.core.model.messaging.MessagePact;
import de.sidion.javamagazin.agiledrivenintegrationstests.ConsumerVerifier;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "person-service-person-producer", providerType = ProviderType.ASYNCH)
class PersonChangedConsumerPactTest {

    @Pact(consumer = "greeting-service-person-consumer")
    MessagePact createPersonWithBirthdayPact(MessagePactBuilder builder) {
        PactDslJsonBody body = new PactDslJsonBody();
        body.integerType("id", 1L)
                .stringType("surname", "Adam")
                .stringType("forename", "Helena")
                .stringType("email", "helena.adam@sidion.de")
                .localDate("birthday", "YYYY-MM-dd", LocalDate.of(1984, 8, 6));

        Map<String, Object> metadata = new HashMap<>();
        metadata.put("Content-Type", "application/json");
        metadata.put("kafka_topic", "person");
        metadata.put("action", "create");
        metadata.put("event", "PersonChanged");
        metadata.put("providerState", "PersonWithBirthday");

        return builder.given("PersonWithBirthday")
                .expectsToReceive("PersonChanged")
                .withMetadata(metadata)
                .withContent(body)
                .toPact();
    }

    @Test
    @PactTestFor(
            pactMethod = "createPersonWithBirthdayPact",
            providerType = ProviderType.ASYNCH,
            pactVersion = PactSpecVersion.V3)
    void verifyPersonWithBirthdayPact(MessagePact messagePact) {
        new ConsumerVerifier("PersonChanged", "PersonWithBirthday").verify(messagePact);
    }
}
