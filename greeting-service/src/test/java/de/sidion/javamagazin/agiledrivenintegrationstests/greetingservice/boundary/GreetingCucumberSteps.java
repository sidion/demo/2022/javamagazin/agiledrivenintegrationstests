package de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.boundary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class GreetingCucumberSteps {

    private static final String BOOTSTRAP_SERVERS = "localhost:9093";

    private static final Path EVENTS_PATH = Paths.get("src", "test", "resources", "events");
    private JsonNode header;
    private JsonNode payload;
    private String response;

    private static Producer<String, String> createKafkaProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }

    @Given("event {word} with provider state {word}")
    public void eventWithProviderState(String event, String providerState) throws IOException {
        var eventPath = EVENTS_PATH.resolve(event).resolve(providerState + ".json");
        var mapper = new ObjectMapper();
        var eventJson = mapper.readTree(eventPath.toFile());
        payload = eventJson.get("payload");
        header = eventJson.get("header");
    }

    @Given("Birthday is today")
    public void birthdayIsToday() {
        ((ObjectNode) payload)
                .put("birthday", LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    @When("event is published and consumed")
    public void eventIsPublishedAndConsumed() throws InterruptedException {
        createKafkaProducer()
                .send(
                        new ProducerRecord<>(
                                "person", 0, "person", payload.toString(), getHeaderSet()));
        Thread.sleep(500);
    }

    @When("I could greet the person with the email {word}")
    public void greetPersonWithEmail(String email) {
        response = given().queryParam("email", email).when().get("/greeting").body().prettyPrint();
    }

    @Then("Greeting goes to {string}")
    public void greetingGoesTo(String name) {
        assertThat(response).isEqualTo("Hello " + name + "!");
    }

    @Then("Birthday greeting goes to {string}")
    public void birthdayGreetingGoesTo(String name) {
        assertThat(response).isEqualTo("Happy Birthday " + name + "!");
    }

    private Set<Header> getHeaderSet() {
        var headerSet = new HashSet<Header>();
        header.fieldNames()
                .forEachRemaining(
                        name ->
                                headerSet.add(
                                        new RecordHeader(
                                                name,
                                                header.get(name)
                                                        .textValue()
                                                        .getBytes(StandardCharsets.UTF_8))));
        return headerSet;
    }
}
