Feature: Birthday Greeting

  Scenario: consume person with birthday event and send birthday greeting
    Given event PersonChanged with provider state PersonWithBirthday
    And Birthday is today
    When event is published and consumed
    And I could greet the person with the email helena.adam@sidion.de
    Then Birthday greeting goes to "Helena Adam"