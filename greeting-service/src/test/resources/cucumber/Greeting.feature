Feature: Greeting

  Scenario: consume person changed event and greet them
    Given event PersonChanged with provider state PersonWithBirthday
    When event is published and consumed
    And I could greet the person with the email helena.adam@sidion.de
    Then Greeting goes to "Helena Adam"