package de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.entity;

import java.time.LocalDate;

public class Person {

    private Long id;
    private String surname;
    private String forename;
    private String email;
    private LocalDate birthday;

    public Person() {}

    public Person(Long id, String surname, String forename, String email, LocalDate birthday) {
        this.id = id;
        this.surname = surname;
        this.forename = forename;
        this.email = email;
        this.birthday = birthday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Person{"
                + "id="
                + id
                + ", surname='"
                + surname
                + '\''
                + ", forename='"
                + forename
                + '\''
                + ", email='"
                + email
                + '\''
                + ", birthday="
                + birthday
                + '}';
    }
}
