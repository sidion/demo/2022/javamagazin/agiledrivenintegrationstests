package de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.boundary;

import de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.control.PersonRepository;
import de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.entity.Person;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.time.MonthDay;

@Path("/greeting")
public class GreetingResource {
    @Inject PersonRepository personRepository;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@QueryParam("id") Long id, @QueryParam("email") String email) {
        if (id != null) {
            var person = personRepository.get(id);
            if (person.isPresent()) {
                return createMessage(person.get());
            }
        }
        if (email != null) {
            var personSet = personRepository.get(email);
            if (!personSet.isEmpty()) {
                return createMessage(personSet.stream().findFirst().get());
            }
        }
        return "Hallo Java Magazin!";
    }

    private String createMessage(Person person) {
        if (todayIsBirthday(person)) {
            return String.format(
                    "Happy Birthday %s %s!", person.getForename(), person.getSurname());
        }
        return String.format("Hello %s %s!", person.getForename(), person.getSurname());
    }

    private boolean todayIsBirthday(Person person) {
        var today = MonthDay.now();
        var birthday =
                MonthDay.of(person.getBirthday().getMonth(), person.getBirthday().getDayOfMonth());
        return today.equals(birthday);
    }
}
