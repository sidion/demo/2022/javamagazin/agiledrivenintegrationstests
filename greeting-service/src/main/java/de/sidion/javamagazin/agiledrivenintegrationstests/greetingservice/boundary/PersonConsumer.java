package de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.boundary;

import de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.control.PersonRepository;
import de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.entity.Person;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PersonConsumer {

    @Inject PersonRepository personRepository;

    @Incoming("person")
    void consumePersonEvent(Person person) {
        if (personRepository.get(person.getId()).isPresent()) {
            personRepository.remove(person.getId());
        }
        personRepository.add(person);
    }
}
