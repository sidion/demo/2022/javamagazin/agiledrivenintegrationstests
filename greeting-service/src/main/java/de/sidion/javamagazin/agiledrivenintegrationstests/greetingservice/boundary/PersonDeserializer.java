package de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.boundary;

import de.sidion.javamagazin.agiledrivenintegrationstests.greetingservice.entity.Person;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class PersonDeserializer extends ObjectMapperDeserializer<Person> {
    public PersonDeserializer() {
        super(Person.class);
    }
}
