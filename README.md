# Wie die CD Pipeline mit CDC und BDD die E2E-Tests ablöst

Demo Projekt für den Java-Magazin-Artikel 'Wie die CD Pipeline mit CDC und BDD die E2E-Tests ablöst' aus Heft 9.2022

Link zur Ausgabe: https://entwickler.de/magazine-ebooks/java-magazin/java-magazin-92022

## Vorbereitung

Für die Demo werden ein paar Services benötigt.
Dafür liegt im root eine *docker-compose.yml*, welche alle nötigen Services enthält.

```shell
docker-compose up
```

#### Postgres

Port: *5432*

Nutzer: *postgres*

Passwort: *password*

pgAdmin: *http://localhost:5050*

pgAdmin-E-Mail: *admin@admin.com*

pgAdmin-Passwort: *root*

#### Kafka

Port: *9093*

AKHQ: *http://localhost:5051/*

#### Pact-Broker

URL: *http://localhost:9292/*

## Start

Für den ersten Lauf wollen wir erst mal die Fehler provozieren...

```shell
mvn clean verify
```

--> error: no pacts found

Zum Beheben, die PACTs im Greeting Service veröffentlichen.

## Greeting Service

### Publish PACTs

```shell
mvn clean verify pact:publish
```

## Person Service

### Verify PACTs

```shell
mvn verify -Dpact.provider.version=1.0-SNAPSHOT -Dpact.verifier.publishResults=true
```