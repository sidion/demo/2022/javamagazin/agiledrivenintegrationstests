package de.sidion.javamagazin.agiledrivenintegrationstests.boundary;

import com.intuit.karate.junit5.Karate;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class PersonKarateTest {

    @Karate.Test
    Karate addPerson() {
        return Karate.run("classpath:karate/Person.feature");
    }
}
