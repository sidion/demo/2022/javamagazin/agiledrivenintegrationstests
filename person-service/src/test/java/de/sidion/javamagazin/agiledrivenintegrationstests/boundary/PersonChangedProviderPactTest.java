package de.sidion.javamagazin.agiledrivenintegrationstests.boundary;

import au.com.dius.pact.provider.MessageAndMetadata;
import au.com.dius.pact.provider.PactVerifyProvider;
import au.com.dius.pact.provider.junit5.MessageTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactBroker;
import au.com.dius.pact.provider.junitsupport.loader.VersionSelector;
import de.sidion.javamagazin.agiledrivenintegrationstests.entity.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Collections;

@PactBroker(url = "http://localhost:9292", consumerVersionSelectors = @VersionSelector())
@Provider("person-service-person-producer")
class PersonChangedProviderPactTest {
    private ProviderVerifier providerVerifier;

    @BeforeEach
    void setUp(PactVerificationContext context) {
        context.setTarget(
                new MessageTestTarget(
                        Collections.singletonList(
                                PersonChangedProviderPactTest.class.getPackageName())));
    }

    @State("PersonWithBirthday")
    public void createPersonWithBirthdayProviderState() {
        providerVerifier = new ProviderVerifier("PersonChanged", "PersonWithBirthday");
    }

    @PactVerifyProvider("PersonChanged")
    public MessageAndMetadata personCreatedEvent() {
        return providerVerifier.toMessageAndMetadata();
    }

    @TestTemplate
    @ExtendWith(PactVerificationInvocationContextProvider.class)
    void testTemplate(PactVerificationContext context) {
        providerVerifier.verify(Person.class);
        context.verifyInteraction();
    }
}
