Feature: Person

  Background:
    * json personRequestBody = read('data/CreatePersonWithBirthday.json')
    * json personWithBirthdayEvent = read('events/PersonChanged/PersonWithBirthday.json')
    * def KafkaTestHelper = Java.type('de.sidion.javamagazin.agiledrivenintegrationstests.boundary.KafkaTestHelper')
    * def kafka = new KafkaTestHelper()


  Scenario: publish person changed event

    Given url applicationBaseUrl
    And path "person"
    And request personRequestBody
    And header Accept = "application/json"
    And set personWithBirthdayEvent.payload.id = '#number'
    When method post
    Then json kafkaEvent = kafka.consume('person')
    And print '### kafka event:', kafkaEvent
    And match kafkaEvent == personWithBirthdayEvent.payload
    And kafka.close()

